"""
Python module to upload images to the abby ocr
"""
import os
import json
import xml.dom.minidom
import logging
import asyncio

from itertools import chain, islice

import aiohttp
import aiofiles


logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
LOGGER = logging.getLogger(__name__)


def chunker(seq, chunk_size, process=iter):
    """ Yields items from an iterator in iterable chunks."""
    iterator = iter(seq)
    while True:
        yield process(
            chain([next(iterator)], islice(iterator, chunk_size - 1)))  # pylint: disable=stop-iteration-return


class ProcessingSettings:  # pylint: disable=too-few-public-methods
    """
    Base settings
    """
    language = "English"
    format = "docx"


class Task:  # pylint: disable=too-few-public-methods
    """
    Task data structure
    """

    def __init__(self):
        self.status = "Unknown"
        self.id = None  # pylint: disable=invalid-name
        self.download_url = None
        self.file_name = None
        self.is_active = self._is_active()

    def _is_active(self):
        return self.status in ("InProgress", "Queued")

    def __str__(self):
        return json.dumps(self.__dict__)

    def __repr__(self):
        return json.dumps(self.__dict__)

    def dict(self):
        """
        A wraper for the magic method
        :return:
        """
        return self.__dict__


class Abby:
    """
    Abby class to send and upload treated documents
    """

    _cached_mapping_name = '_cached_mapping.json'
    _current_path = os.getcwd()

    base_url = 'https://cloud.ocrsdk.com/'
    chunk_size = 10

    __slots__ = (
        '_destination_folder', 'input_file',
        'input_folder', 'mapping', 'loop', 'session',
        'download_session'
    )

    def __init__(self, destination_folder, input_file, input_folder, login, pwd):
        self._destination_folder = None
        self.destination_folder = destination_folder
        self.input_file = input_file
        self.input_folder = input_folder
        self.loop = asyncio.get_event_loop()
        self.mapping = self.get_cached_mapping()

        self.session = aiohttp.ClientSession(
            auth=aiohttp.helpers.BasicAuth(login, pwd),
            loop=self.loop
        )

        self.download_session = aiohttp.ClientSession(
            loop=self.loop
        )

    async def _clean_sessions(self):
        """
        Method to close sessions from the init
        :return:
        """
        if self.session:
            await self.session.close()
        if self.download_session:
            await self.download_session.close()

    def close(self):
        """
        Close loop and sessions
        :return:
        """
        self.loop.run_until_complete(self._clean_sessions())
        if self.loop:
            self.loop.close()

    def get_cached_mapping(self):
        """
        Check if there are unclosed files
        :return:
        """
        if os.path.exists(self._cached_mapping_name):
            with open(self._cached_mapping_name) as cached_mapping:
                return json.load(cached_mapping)
        return {}

    def run(self):
        """
        An entry point of the class
        :return:
        """
        try:
            executor = self.get_start_function()
            self.loop.run_until_complete(executor())
        except:  # pylint: disable=bare-except
            with open(self._cached_mapping_name, 'w') as cached_session:
                json.dump(self.mapping, cached_session)

    def get_start_function(self):
        """
        Choose a method to start
        (continue from the last one or start a new one)
        :return:
        """
        if self.mapping:
            LOGGER.info('continue')
            function = self.save_finished_files
        else:
            LOGGER.info('new one')
            function = self.send_files
        return function

    async def save_file(self, task, file_name):
        """
        Get file from the api and save it to the folder
        :param task:
        :param file_name:
        :return:
        """
        path = os.path.join(self.destination_folder, file_name)
        LOGGER.info(task)
        async with aiofiles.open(path, 'wb', loop=self.loop) as destination_file, \
                self.download_session.get(task.download_url) as resp:
            resp.raise_for_status()
            while True:
                response_chunk = await resp.content.read(1024)
                if not response_chunk:
                    break
                await destination_file.write(response_chunk)
            await self.delete_task(task.id)

    async def save_finished_files(self, check_mapping=True):
        """
        Get all the processed files and save them
        :param check_mapping:
        :return:
        """
        cnt = 100
        while True:
            asyncio.sleep(5)
            cnt -= 1
            LOGGER.info(cnt)
            tasks = await self.get_finished_tasks()
            LOGGER.info(tasks)
            coros = []
            for task in tasks:
                file_name = self.mapping.pop(task.id, None)
                if file_name:
                    coros.append(self.save_file(task, file_name))
            await asyncio.gather(*coros)
            if not self.mapping or not check_mapping or not cnt:
                LOGGER.info('Break')
                break

    async def send_files(self):
        """
        Upload file to the api
        :return:
        """
        with open(self.input_file) as input_file:
            for chunk in chunker(input_file, self.chunk_size):
                await asyncio.gather(*[
                    self.send_file(
                        os.path.join(self.input_folder, file_name.strip()), file_name
                    )
                    for file_name in chunk
                ])
                await self.save_finished_files(check_mapping=False)
            await self.save_finished_files(check_mapping=True)

    async def send_file(self, path, file_name):
        """
        Upload file to the api
        :param path:
        :param file_name:
        :return:
        """
        data = await self.read_file(path)
        url_params = {
            "language": ProcessingSettings.language,
            "exportFormat": ProcessingSettings.format
        }
        request_url = self.base_url + "processImage"
        async with self.session.post(request_url, data=data, params=url_params) as resp:
            resp.raise_for_status()
            res_data = await resp.read()
            LOGGER.info(res_data)
            task = self.decode_response(res_data)[0]
            task.file_name = file_name + '.docx'
            self.mapping[task.id] = file_name + '.docx'
        return task

    async def read_file(self, path):
        """
        Read file from disk
        :param path:
        :return:
        """
        async with aiofiles.open(path, 'rb', loop=self.loop) as image_file:
            data = await image_file.read()
            return data

    async def get_finished_tasks(self):
        """
        Execute query to get all the processed files
        :return:
        """
        request_url = self.base_url + "listFinishedTasks"
        async with self.session.get(request_url) as resp:
            data = await resp.read()
            return self.decode_response(data)

    async def delete_task(self, task_id):
        """
        Remove a file from the api
        :param task_id:
        :return:
        """
        request_url = self.base_url + "deleteTask"
        async with self.session.get(request_url, params={'taskId': task_id}) as resp:
            resp.raise_for_status()
            data = await resp.read()
            return self.decode_response(data)[0]

    def decode_response(self, xml_response):  # pylint: disable=no-self-use
        """ Decode xml response of the server. Return Task object """
        dom = xml.dom.minidom.parseString(xml_response)
        task_nodes = dom.getElementsByTagName("task")
        res = []
        for task_node in task_nodes:
            task = Task()
            task.id = task_node.getAttribute("id")
            task.status = task_node.getAttribute("status")
            if task.status == "Completed":
                task.download_url = task_node.getAttribute("resultUrl")
            res.append(task)
        return res

    @property
    def destination_folder(self):
        """
        destination folder getter
        :return:
        """
        return self._destination_folder

    @destination_folder.setter
    def destination_folder(self, folder_path):
        """
        Destination folder setter
        :param v:
        :return:
        """
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
        self._destination_folder = folder_path


def main():
    """
    Entry point of this file

    :return:
    """

    # collect user input

    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "input_file",
        help="Full or relative path to the input file",
        type=str
    )
    parser.add_argument(
        "input_folder",
        help="Full or relative path to the input folder",
        type=str
    )

    parser.add_argument(
        "destination_folder",
        help="a full path to the destination folder",
        type=str
    )

    args = parser.parse_args()
    destination_folder = args.destination_folder
    input_file = args.input_file
    input_folder = args.input_folder
    #  Execute program
    import contextlib
    # example python abby_ocr.py filelist.txt(file with a list of files) ocr-set(foler with pdf) results (folder for results)
    with contextlib.closing(
        Abby(destination_folder, input_file, input_folder, login=None, pwd=None)
    ) as abby:
        abby.run()  # pylint: disable=no-member


if __name__ == '__main__':
    main()
